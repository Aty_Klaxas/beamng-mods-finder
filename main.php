<?php

require_once __DIR__ . '/lib/vendor/autoload.php';

use League\CLImate\CLImate;
use League\CLImate\TerminalObject\Dynamic\Progress;

function myscan(string $dir): array
{
    $return = [];

    foreach (scandir($dir) as $item) {
        if ($item === '.' || $item === '..') {
            continue;
        }

        $return[] = $dir . '/' . $item;
    }

    return $return;
}

function tree(string $dir): array
{
    $dirs = [$dir];
    $files = [];

    do {
        $scan = myscan(array_shift($dirs));

        foreach ($scan as $a_path) {
            if (is_dir($a_path)) {
                $dirs[] = $a_path;
            } else {
                $files[] = $a_path;
            }
        }
    } while (!empty($dirs));

    return $files;
}

function human_filesize($bytes, $decimals = 2) {
    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
}

/*
function array_map_recursive($callback, $array)
{
    $func = function ($item) use (&$func, &$callback) {
        return is_array($item) ? array_map($func, $item) : call_user_func($callback, $item);
    };

    return array_map($func, $array);
}

function see($variable, string $text = '')
{
    echo "\n\n<br><br>############### $text ###############<br>\n<pre style='white-space: pre'>" . PHP_EOL;

    if ($variable instanceof DBStatement) {
        $variable->debugDumpParams();
        echo "\n<br>";
    }

    $limit = 10000;

    if (is_array($variable)) {
        $variable = array_map_recursive(static function($e) use ($limit) {
            if (!is_string($e) || strlen($e) < $limit) {
                return $e;
            }

            return 'string > ' . $limit . ' char long, cuted in the see';
        }, $variable);
    }

    var_dump($variable);
    echo '</pre>';
}

function array_diff_double(array $a, array $b) {
    return array_merge(array_diff($a, $b), array_diff($b, $a));
}

function array_filter_recursive($input, $callback)
{
    foreach ($input as &$value) {
        if (is_array($value)) {
            $value = array_filter_recursive($value, $callback);
        }
    }

    return array_filter($input, $callback);
}

function double_explode(string $to_explode, string $glue1, string $glue2)
{
    $exp = explode($glue1, $to_explode);

    return array_map(static function ($x) use ($glue2) {
        return explode($glue2, $x);
    }, $exp);
}

function map_count(array $arr)
{
    return array_map('count', $arr);
}

function my_substr(string $str, int $start, ?int $end = null)
{
    if ($end === null) {
        return substr($str, $start);
    }

    return substr($str, $start, $end - $start);
}

function make_group(string $input, string $open, string $close) {
    $array_pos = [];
    $pos_open = -1;
    $pos_close = -1;

    do {
        $pos_open = strpos($input, $open, $pos_open + 1);

        if ($pos_open !== false) {
            $array_pos[$pos_open] = $open;
        }
    } while($pos_open !== false);

    do {
        $pos_close = strpos($input, $close, $pos_close + 1);

        if ($pos_close !== false) {
            $array_pos[$pos_close] = $close;
        }
    } while($pos_close !== false);

    ksort($array_pos);

    $keys = array_keys($array_pos);
    $count = count($keys);
    $groups = [];

    for ($i = 0; $i < $count; $i++) {
        $an_pos_A = $keys[$i];
        $type_A = $array_pos[$an_pos_A];

        if ($type_A === $close) {
            continue;
        }

        $sum = 0;

        for ($j = $i; $j < $count; $j++) {
            $an_pos_B = $keys[$j];
            $type_B = $array_pos[$an_pos_B];

            if ($type_B === $close) {
                $sum--;

                if ($sum === 0) {
                    break;
                }
            } elseif ($type_B === $open) {
                $sum++;
            }
        }

        $groups[] = [
            'open' => $an_pos_A,
            'close' => $an_pos_B,
        ];
    }

    $keys = array_keys($groups);
    $count = count($keys);

    for ($i = 0; $i < $count; $i++) {
        $keyA = $keys[$i];
        $value_enfant = $groups[$keyA];
        $finded = false;

        for ($j = 0; $j < $i; $j++) {
            $keyB = $keys[$j];
            $value_parent = $groups[$keyB];

            if ($value_parent['close'] < $value_enfant['open']) {
                continue;
            }

            $finded = true;
            $groups[$i]['from'] = $j;
        }

        if ($finded === false) {
            $groups[$i]['from'] = null;
        }
    }

    foreach ($groups as $id => $group) {
        $group['id'] = $id;
        $groups[$id] = $group;
    }

    return $groups;
}

function parse_raw_value(string $input)
{
    $return = null;
    $expl = explode('"', $input);

    $first = array_shift($expl);
    $first = str_replace(':', '', $first);

    if (trim($first) === "" && trim(array_pop($expl)) === "") {
        foreach ($expl as $l => $exp) {
            if (
                substr($exp, -1) === '\\' ||
                count($expl) === ($l + 1)
            ) {
                continue;
            }

            throw new Exception('Syntax error : (' . $input . ')');
        }

        $return = str_replace('\\"', '"', implode('"', $expl));
    } else {
        $intval = (int) $input;
        $floatval = (float) $input;
        $floatintval = (float) $intval;

        $return = $floatintval === $floatval ? $intval : $floatval;
    }

    return $return;
}

function str_replace_once($search, $replace, $subject)
{
    $pos = strpos($subject, $search);

    if ($pos === false) {
        return $subject;
    }

    return substr_replace($subject, $replace, $pos, strlen($search));
}
*/

// =====================================================================================================================

class STATIC_DATA {

    public static $DATA = [];

    public static function file_get_contents(string $file)
    {
        if (!isset(self::$DATA[$file])) {
            self::$DATA[$file] = file_get_contents($file);
        }

        return self::$DATA[$file];
    }

    public static function tree(string $dir)
    {
        if (!isset(self::$DATA[$dir])) {
            self::$DATA[$dir] = tree($dir);
        }

        return self::$DATA[$dir];
    }

}

// =====================================================================================================================

$climate = new CLImate();
$climate->description('Programme de detection des pieces des voiture de BeamNG');
$climate->arguments->add([
    'vehicules' => [
        'longPrefix' => 'vehicules',
        'defaultValue' => 'vehicules',
        'description' => 'Where to search .pc files, or specific .pc file',
    ],
    'mods' => [
        'longPrefix' => 'mods',
        'defaultValue' => 'mods',
        'description' => 'Where to search mods files',
    ]
]);

// climate parsing, if it throw an error, $e will be filled
try {
    $climate->arguments->parse();
} catch (Throwable $e) {}

$vehicules_folder = $climate->arguments->get('vehicules');
$mods_folder = $climate->arguments->get('mods');

if ($vehicules_folder[0] !== '/') {
    $vehicules_folder = __DIR__ . '/' . $vehicules_folder;
}

if ($mods_folder[0] !== '/') {
    $mods_folder = __DIR__ . '/' . $mods_folder;
}

$climate->bold()->yellow('Vehicules folder ' . $vehicules_folder);
$climate->bold()->yellow('Mods folder ' . $mods_folder);

// ======================================================================================================

$vehicules = [];
$mods = [];

if (!file_exists($vehicules_folder)) {
    $climate->red('Vehicule folder does not exist');
    exit;
}

if (!file_exists($mods_folder)) {
    $climate->red('Mods folder does not exist');
    exit;
}

if (!is_dir($vehicules_folder)) {
    $vehicules[] = $vehicules_folder;
} else {
    if (substr($vehicules_folder, -1) === '/') {
        $vehicules_folder = substr($vehicules_folder, 0, -1);
    }

    $dirs = [$vehicules_folder];

    do {
        $scan = array_values(array_diff(scandir(array_shift($dirs)), ['.', '..']));
        $path = $vehicules_folder;

        foreach ($scan as $item) {
            $path_loop = $path . '/' . $item;

            if (is_dir($path_loop)) {
                $dirs[] = $path_loop;
            } else {
                $vehicules[] = $path_loop;
            }
        }
    } while (!empty($dirs));

    $vehicules = array_filter($vehicules, static function($x) {
        return pathinfo($x, PATHINFO_EXTENSION) === 'pc';
    });
}

if (!is_dir($mods_folder)) {
    $mods[] = $mods_folder;
} else {
    $mods = array_diff(scandir($mods_folder), ['.', '..']);

    if (substr($mods_folder, -1) === '/') {
        $mods_folder = substr($mods_folder, 0, -1);
    }

    $mods = array_map(static function($x) use ($mods_folder) {
        return $mods_folder . '/' . $x;
    }, $mods);

    $mods = array_filter($mods, static function($x) {
        return pathinfo($x, PATHINFO_EXTENSION) === 'zip';
    });
}

// ======================================================================================================
// unzip if possible

$climate->out('Unzip mods');
$mods = array_values($mods);
/** @var Progress $progress */
$progress = $climate->progress()->total(count($mods));
$total_filesize = 0;
$mods_folders = [];

foreach ($mods as $z => $mod) {
    $total_filesize += filesize($mod);
}

$remaning_filesize = $total_filesize;
$actual_filesize = 0;

foreach ($mods as $z => $mod) {
    $filesize = filesize($mod);
    $remaning_filesize -= $filesize;
    $actual_filesize += $filesize;
    $progress->current($z,
        $mod . ' ' .
        human_filesize($filesize) . ' ' .
        human_filesize($actual_filesize) . '/' .
        human_filesize($total_filesize) . ' ' .
        human_filesize($remaning_filesize)
    );
    $dest = __DIR__ . '/extract/' . pathinfo($mod, PATHINFO_BASENAME);
    $mods_folders[] = $dest;

    if (file_exists($dest) && !is_dir($dest)) {
        unlink($dest);
    }

    $zip = new ZipArchive();
    $zip->open($mod);

    if (!file_exists($dest)) {
        $zip->extractTo($dest);
        $zip->close();

        continue;
    }

    $tree_dest = tree($dest);
    $zip_files = [];

    for ($i = 0; $i < $zip->numFiles; $i++) {
        $stat = $zip->statIndex($i);

        if (substr($stat['name'], -1) === '/') {
            continue;
        }

        $zip_files[] = $dest . '/' . $stat['name'];
    }

    $missing_in_files_from_zip = array_diff($zip_files, $tree_dest);

    if (empty($missing_in_files_from_zip)) {
        continue;
    }

    foreach ($missing_in_files_from_zip as $file_dest) {
        $zip_path = substr($file_dest, strlen($dest) + 1);

        $zip->extractTo($dest, $zip_path);
    }
}

$progress->current($z + 1, 'OK');

// ======================================================================================================

$vehicules_content = [];

foreach ($vehicules as $a_vehicule) {
    $data = json_decode(file_get_contents($a_vehicule), true);
    $vehicules_content[pathinfo($a_vehicule, PATHINFO_BASENAME)] = $data['parts'];
}

// ======================================================================================================

$vanilla_vehicules = tree(__DIR__ . '/vanilla_vehicules');

// ======================================================================================================

$count_vehicule = count($vehicules_content);
$count_mods = count($mods_folders);
$count_vehicules_content = [];

foreach ($vehicules_content as $vehicule_file => $a_vehicule_content) {
    $count_vehicules_content[$vehicule_file] = count($a_vehicule_content);
}

$climate->out('Searching mods...');
/** @var Progress $progress */
$progress = $climate->progress()->total($count_vehicule + array_sum($count_vehicules_content));
$progress->current(0, 'Starting');

// ======================================================================================================

$vehicules_mods = [];
$index_vehicules_file = 0;
$index_vehicules_content = 0;

foreach ($vehicules_content as $vehicule_file => $a_vehicule_content) {
    $progress_text = ($index_vehicules_file + 1) . '/' . $count_vehicule . ' ' . $vehicule_file;
    $progress->current($index_vehicules_file + $index_vehicules_content, $progress_text);

    // loop on every part of this vehicule
    foreach ($a_vehicule_content as $vehicule_part_name => $vehicule_part) {
        $progress->current($index_vehicules_file + $index_vehicules_content, $progress_text . ' ' . $vehicule_part);
        $index_vehicules_content++;

        // skip empty parts
        if (empty($vehicule_part)) {
            continue;
        }

        $vanilla = false;

        foreach ($vanilla_vehicules as $vanilla_vehicules_file) {
            $ext = pathinfo($vanilla_vehicules_file, PATHINFO_EXTENSION);

            // arbitrary
            if (!in_array(strtolower($ext), ['json', 'pc', 'jbeam'], true)) {
                continue;
            }

            // get content in static variable
            $content = STATIC_DATA::file_get_contents($vanilla_vehicules_file);

            // test if empty
            if (empty($content)) {
                continue;
            }

            // if part does not exist in this vanilla vehicule file
            // search in next vehicule file
            if (strpos($content, $vehicule_part) === false) {
                continue;
            }

            // if part exist in this vanilla vehicule file
            // this is vanilla file
            $vanilla = true;
            break;
        }

        if ($vanilla === true) {
            // make nothing with vanilla part
            continue;
        }

        // not vanilla part only

        foreach ($mods_folders as $mods_folder) {
            // get content in static variable
            $tree = STATIC_DATA::tree($mods_folder);
            $find_in_this_mod = false;

            foreach ($tree as $mods_file) {
                $ext = pathinfo($mods_file, PATHINFO_EXTENSION);

                // arbitrary
                if (!in_array(strtolower($ext), ['json', 'pc', 'jbeam'], true)) {
                    continue;
                }

                // get content in static variable
                $content = STATIC_DATA::file_get_contents($mods_file);

                // test if empty
                if (empty($content)) {
                    continue;
                }

                // if part does not exist in this vanilla vehicule file
                // search in next vehicule file
                if (strpos($content, $vehicule_part) === false) {
                    continue;
                }

                $find_in_this_mod = true;
                break;
            }

            if ($find_in_this_mod === false) {
                continue;
            }

            $mods_file = pathinfo($mods_folder, PATHINFO_BASENAME);
            $progress->current($index_vehicules_file + $index_vehicules_content, $progress_text . ' ' . $vehicule_part . ' ' . $mods_file);
            $vehicules_mods[$vehicule_file][] = $mods_file;
        }

        $progress->current($index_vehicules_file + $index_vehicules_content, $progress_text . ' OK !');
    }

    $index_vehicules_file++;
}

$progress->current($index_vehicules_file + $index_vehicules_content, 'Done !');

// =====================================================================================================================

$table = [];

foreach ($vehicules_mods as $vehicules_name => $vehicules_mod_list) {
    $vehicules_mod_list = array_unique($vehicules_mod_list);

    $table[] = [
        $vehicules_name,
        array_shift($vehicules_mod_list)
    ];

    foreach ($vehicules_mod_list as $item) {
        $table[] = [
            '',
            $item
        ];
    }
}

$climate->bold()->green('Vehicules files with related Mods');
$climate->table($table);

$climate->bold()->green('Done');

