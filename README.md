# BeamNG mods finder

BeamNG mods finder is simple as we can read the title  
I did this project because I couldn't find a way to get the mod information for a vehicle  
I thought the project to be simple to develop and to do the job despite the performance

## Problem

My BeamNG game is full of all kinds of mods  
In these mods, I have mods that add parts for my vehicles  
I would like to know which mod goes with which vehicle  

## How it works

1. Create folders `vanilla_vehicules` `mods` `vehicules` `extract`
2. Find your BeamNG installation and appdata files (  
Steam BeamNG folder: `C:\Program Files (x86)\Steam\steamapps\common\BeamNG.drive`  
Appdata: `C:\Users\<USER>\AppData\Local\BeamNG.drive\<VERSION>\mods`)
3. Copy folder content `BeamNG.drive/content/vehicles` into `vanilla_vehicules` and unzip all files
4. Copy all your mods (`C:\Users\<USER>\AppData\Local\BeamNG.drive\<VERSION>\mods`) and **don't** unzip all files
5. Copy your .pc vehicle files in `vehicules`
6. Run program `php main.php` 

## Result

```
Vehicules folder /Users/<USER>/beamng-mods-finder/vehicules
Mods folder /Users/<USER>/beamng-mods-finder/mods
Unzip mods
======================================================================> 100%
OK
Searching mods...
======================================================================> 100%
Done !
Vehicules files with related Mods
--------------------------
| vehicle1.pc | mod1.zip |
--------------------------
|             | mod2.zip |
--------------------------
| vehicle2.pc | mod1.zip |
--------------------------
|             | mod3.zip |
--------------------------
Done 
```
